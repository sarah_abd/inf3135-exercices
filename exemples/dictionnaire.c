#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NB_LETTRES  26
#define LNG_MAX_MOT 30

// --------------------- //
// Structures de données //
// --------------------- //

struct Noeud {                         // Un noeud du dictionnaire
    bool terminal;                     // Vrai si le noeud correspond a un mot
    struct Noeud *enfants[NB_LETTRES]; // Un tableau de pointeurs de noeuds
};

struct Dictionnaire {     // Un dictionnaire
    struct Noeud *racine; // La racine du dictionnaire
    unsigned int nbMots;  // Le nombre de mots dans le dictionnaire
};

// ---------- //
// Prototypes //
// ---------- //

/**
 * Retourne un dictionnaire vide.
 *
 * @return  Le dictionnaire
 */
struct Dictionnaire Dictionnaire_creer();

/**
 * Affiche le contenu d'un dictionnaire sur stdout.
 *
 * @return  Le dictionnaire à afficher
 */
void Dictionnaire_afficher(const struct Dictionnaire *dictionnaire);

/**
 * Insère un mot dans un dictionnaire.
 *
 * Si le mot est déjà présent, ne fait rien.
 *
 * @param dictionnaire  Le dictionnaire dans lequel on insère
 * @param mot           Le mot a insérer
 */
void Dictionnaire_insererMot(struct Dictionnaire *dictionnaire,
                             const char *mot);

/**
 * Indique si un mot se trouve dans un dictionnaire
 *
 * @param dictionnaire  Le dictionnaire à consulter
 * @param mot           Le mot à vérifier
 * @return              Vrai si et seulement si mot est dans dictionnaire
 */
bool Dictionnaire_contientMot(const struct Dictionnaire *dictionnaire,
                              const char *mot);

/**
 * Supprime un mot d'un dictionnaire.
 *
 * Si le mot n'est pas présent, ne fait rien.
 *
 * @param dictionnaire  Le dictionnaire
 * @param mot           Le mot à supprimer
 */
void Dictionnaire_supprimerMot(struct Dictionnaire *dictionnaire,
                               const char *mot);

/**
 * Détruit un dictionnaire.
 *
 * @param dictionnaire  Le dictionnaire à détruire
 */
void Dictionnaire_detruire(struct Dictionnaire *dictionnaire);

// -------------- //
// Implementation //
// -------------- //

struct Dictionnaire Dictionnaire_creer() {
    struct Dictionnaire dictionnaire = {.racine = NULL,
                                        .nbMots = 0};
    return dictionnaire;
}

/**
 * Affiche récursivement le contenu d'un noeud.
 *
 * @param noeud       Le noeud à partir duquel on fait l'affichage
 * @param prefixe     Le mot correpondant au noeud courant
 * @param profondeur  La profondeur du noeud courant
 */
void Dictionnaire_afficherRecursif(const struct Noeud *noeud,
                                   char prefixe[LNG_MAX_MOT],
                                   unsigned int profondeur) {
    if (noeud != NULL) {
        if (noeud->terminal) printf("%s\n", prefixe);
        for (unsigned int i = 0; i < NB_LETTRES; ++i) {
            prefixe[profondeur] = 'a' + i;
            prefixe[profondeur + 1] = '\0';
            Dictionnaire_afficherRecursif(noeud->enfants[i],
                                          prefixe, profondeur + 1);
        }
    }
}

void Dictionnaire_afficher(const struct Dictionnaire *dictionnaire) {
    char prefixe[LNG_MAX_MOT + 1] = "";
    Dictionnaire_afficherRecursif(dictionnaire->racine, prefixe, 0);
}

/**
 * Insère récursivement un mot dans un dictionnaire.
 *
 * @param noeud    Le noeud courant
 * @param suffixe  La portion de mot pas encore lue
 */
void Dictionnaire_insererMotRecursif(struct Noeud **noeud,
                                     const char *suffixe) {
    if (*noeud == NULL) {
        *noeud = malloc(sizeof(struct Noeud));
        (*noeud)->terminal = false;
        for (unsigned int i = 0; i < NB_LETTRES; ++i) {
            (*noeud)->enfants[i] = NULL;
        }
    }
    if (strcmp(suffixe, "") == 0) {
        (*noeud)->terminal = true;
    } else {
        unsigned int i = suffixe[0] - 'a';
        Dictionnaire_insererMotRecursif(&(*noeud)->enfants[i], suffixe + 1);
    }
}

void Dictionnaire_insererMot(struct Dictionnaire *dictionnaire,
                             const char *mot) {
    Dictionnaire_insererMotRecursif(&dictionnaire->racine, mot);
}

/**
 * Vérifie récursivement la présence d'un mot dans un dictionnaire.
 *
 * @param noeud    Le noeud courant
 * @param suffixe  La portion de mot pas encore lue
 */
bool Dictionnaire_contientMotRecursif(const struct Noeud *noeud,
                                      const char *suffixe) {
    if (noeud == NULL) {
        return false;
    } else if (strcmp(suffixe, "") == 0) {
        return noeud->terminal;
    } else {
        unsigned int i = suffixe[0] - 'a';
        return Dictionnaire_contientMotRecursif(noeud->enfants[i],
                                                suffixe + 1);
    }
}

bool Dictionnaire_contientMot(const struct Dictionnaire *dictionnaire,
                              const char *mot) {
    return Dictionnaire_contientMotRecursif(dictionnaire->racine, mot);
}

/**
 * Supprime récursivement un mot d'un dictionnaire.
 *
 * @param noeud    Le noeud courant
 * @param suffixe  La portion de mot pas encore lue
 */
void Dictionnaire_supprimerMotRecursif(struct Noeud *noeud,
                                       const char *suffixe) {
    if (noeud != NULL) {
        if (strcmp(suffixe, "") == 0) {
            noeud->terminal = false;
        } else {
            unsigned int i = suffixe[0] - 'a';
            Dictionnaire_supprimerMotRecursif(noeud->enfants[i],
                                              suffixe + 1);
        }
    }
}

void Dictionnaire_supprimerMot(struct Dictionnaire *dictionnaire,
                               const char *mot) {
    Dictionnaire_supprimerMotRecursif(dictionnaire->racine, mot);
}

/**
 * Détruit récursivement un dictionnaire.
 *
 * @param noeud  Le noeud courant
 */
void Dictionnaire_detruireRecursif(struct Noeud *noeud) {
    if (noeud != NULL) {
        for (unsigned int i = 0; i < NB_LETTRES; ++i) {
            Dictionnaire_detruireRecursif(noeud->enfants[i]);
        }
        free(noeud);
    }
}

void Dictionnaire_detruire(struct Dictionnaire *dictionnaire) {
    Dictionnaire_detruireRecursif(dictionnaire->racine);
}

// ---------------------- //
// Affichage comme graphe //
// ---------------------- //

/**
 * Retourne un mot tel quel ou "racine" si le mot est vide.
 *
 * Note: Cette fonction est utile pour le format dot.
 *
 * @param mot  Le mot à transformer
 * @return     Le même mot ou "racine" si le mot est vide
 */
const char *motDot(const char *mot) {
    if (strlen(mot) == 0) {
        return "racine";
    } else {
        return mot;
    }
}

/**
 * Affiche récursivement le sous-arbre d'un noeud au format dot.
 *
 * @param noeud       Le noeud courant
 * @param prefixe     Le mot correspondant au noeud courant
 * @param profondeur  La profondeur du noeud
 */
void Dictionnaire_printDotRecursive(struct Noeud *noeud,
                                    char prefixe[LNG_MAX_MOT],
                                    unsigned int profondeur) {
    int c = profondeur == 0 ? ' ' : prefixe[profondeur - 1];
    if (noeud != NULL) {
        if (noeud->terminal) {
            printf("  %s [label=\"%c\", fontsize=24, fixedsize=true, peripheries=2];\n",
                   motDot(prefixe), c);
        } else {
            printf("  %s [label=\"%c\", fontsize=24, fixedsize=true];\n", motDot(prefixe), c);
        }
        for (unsigned int i = 0; i < NB_LETTRES; ++i) {
            char mot[LNG_MAX_MOT];
            strncpy(mot, prefixe, LNG_MAX_MOT);
            mot[profondeur] = 'a' + i;
            mot[profondeur + 1] = '\0';
            if (noeud->enfants[i] != NULL) {
                printf("  %s -> %s;\n", motDot(prefixe), mot);
                Dictionnaire_printDotRecursive(noeud->enfants[i],
                                               mot, profondeur + 1);
            }
        }
    }
}

/**
 * Affiche le dictionnaire sur stdout au format dot.
 *
 * Remarque: Le format dot est celui reconnu par Graphviz. Cela facilite la
 * visualisation de la structure de données, qui est un arbre.
 *
 * @param dictionnaire  Le dictionnaire à afficher
 */
void Dictionnaire_printDot(const struct Dictionnaire *dictionnaire) {
    printf("digraph {\n");
    char prefixe[LNG_MAX_MOT + 1] = "";
    Dictionnaire_printDotRecursive(dictionnaire->racine,
                                   prefixe,
                                   0);
    printf("}\n");
}

// ---- //
// Main //
// ---- //

void construireDictionnaire(struct Dictionnaire *dictionnaire) {
    *dictionnaire = Dictionnaire_creer();
    Dictionnaire_insererMot(dictionnaire, "frais");
    Dictionnaire_insererMot(dictionnaire, "banane");
    Dictionnaire_insererMot(dictionnaire, "froment");
    Dictionnaire_insererMot(dictionnaire, "bandit");
    Dictionnaire_insererMot(dictionnaire, "bar");
    Dictionnaire_insererMot(dictionnaire, "fromage");
    Dictionnaire_insererMot(dictionnaire, "ananas");
    Dictionnaire_insererMot(dictionnaire, "barrer");
    Dictionnaire_insererMot(dictionnaire, "fraise");
    Dictionnaire_insererMot(dictionnaire, "bar");
    Dictionnaire_insererMot(dictionnaire, "ange");
    Dictionnaire_insererMot(dictionnaire, "froment");
    Dictionnaire_supprimerMot(dictionnaire, "ananas");
}

void test() {
    struct Dictionnaire dictionnaire;
    construireDictionnaire(&dictionnaire);
    Dictionnaire_afficher(&dictionnaire);
    printf("\"ananas\" dans dictionnaire? %s\n",
           Dictionnaire_contientMot(&dictionnaire, "ananas") ?
           "oui" : "non");
    printf("\"tomate\" dans dictionnaire? %s\n",
           Dictionnaire_contientMot(&dictionnaire, "tomate") ?
           "oui" : "non");
    printf("\"anana\" dans dictionnaire? %s\n",
           Dictionnaire_contientMot(&dictionnaire, "anana") ?
           "oui" : "non");
    Dictionnaire_detruire(&dictionnaire);
}

void testDot() {
    struct Dictionnaire dictionnaire;
    construireDictionnaire(&dictionnaire);
    Dictionnaire_printDot(&dictionnaire);
    Dictionnaire_detruire(&dictionnaire);
}

int main() {
    testDot();
    //test();
    return 0;
}
