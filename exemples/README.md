# Exemples

Ce dossier contient des fichiers complémentaires aux séances de laboratoire.

## Fichier `.vimrc`

Il s'agit d'un fichier de configuration de base pour Vim. Noter que pour le
faire fonctionner correctement, il est important d'installer
[Vim-plug](https://github.com/junegunn/vim-plug). Il est important d'enrichir
ce fichier selon vos besoins, mais il constitue un bon point de départ pour le
cours.

## Fichier `.tmux.conf`

Il s'agit aussi d'un fichier de configuration de base pour le multiplexeur de
terminal [Tmux](https://github.com/tmux/tmux/wiki). Je l'utilise principalement
pour ouvrir plusieurs fenêtres dans un même terminal.

## Fichier `.profile`

Quelques raccourcis que j'ajoute à mon fichier `.profile` (sous Linux, c'est
plutôt le fichier `.bashrc`) pour modifier l'invite de commande, pour ajouter
dans la variable `PATH` le chemin vers mes scripts personnels, etc.

## Fichiers C

Quelques fichiers C utiles pour compléter certaines séances de laboratoire.
